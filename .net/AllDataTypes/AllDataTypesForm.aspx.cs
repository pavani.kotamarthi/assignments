﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AllDataTypes
{
    public partial class AllDataTypesForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Insert_Click(object sender, EventArgs e)
        {
            var insertAllDataTypes = new AllDataTypesTable
            {
                Id = 2,
                Name = "third",
                DateOfBirth = Convert.ToDateTime("10/10/2010"),
                Salary = 50000,
                PhoneNo = 123456789,
                Age = 23,
                Gender = "M",
                Experience = 2.2
            };
            AllDataTypesDBEntities allDataTypesDBEntities = new AllDataTypesDBEntities();
            allDataTypesDBEntities.AllDataTypesTables.Add(insertAllDataTypes);
            allDataTypesDBEntities.SaveChanges();
        }
    }
}