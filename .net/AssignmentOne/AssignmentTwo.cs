﻿using System;

public class AssignmentTwo
{
	public static int count = 0;
	public AssignmentTwo()
	{ 
		Console.WriteLine("Hello assignment2");
	}
	public void addFloatBinary()
    {
		count++;
		String bin1 = Console.ReadLine();
		String bin2 = Console.ReadLine();
		float firstnum = convertingPart(bin1);
		float secondnum = convertingPart(bin2);
		Console.WriteLine(firstnum +","+ secondnum);
		Console.WriteLine(firstnum + secondnum);
    }
	public float convertingPart(String number)
    {
		count++;
		int point = number.IndexOf('.');
		float twos = 1;
		float leftpart = 0;
		for (int i = point - 1; i >= 0; i--)
		{
			leftpart += (number[i] - '0') * twos;
			twos *= 2;
		}
		float fractionpart = 0;
		twos = 2;
		for (int j = point + 1; j < number.Length; j++)
		{
			fractionpart += (number[j] - '0') / twos;
			twos *= 2;
		}
		return (leftpart + fractionpart);
	} 
}

