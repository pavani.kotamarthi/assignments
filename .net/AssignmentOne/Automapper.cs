﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentOne
{
    class Automapper1<T1,T2>
    {
        public T1 Name { get; set; }
        public T2 Age { get; set; }
    }
    class Automapper2<T1, T2>
    {
        public T1 Name { get; set; }
        public T2 Age { get; set; }
    }
}
