﻿using System;
using System.Threading;
using AutoMapper;

namespace AssignmentOne
{
    class ReadingNumbers
    {
        public void AddNumbers()
        {
            String val1,val2;
            val1 = Console.ReadLine();
            val2 = Console.ReadLine();
            String[] numbersArray = new String[2];
            numbersArray[0] = val1;
            numbersArray[1] = val2;
            int sum = 0;
            for(int i = 0; i < numbersArray.Length; i++)
            {
                sum = sum + Convert.ToInt32(numbersArray[i]);
            }
            Console.WriteLine(sum);
        }
        /*public void AddNums()
        {
            int value1, value2;
            value1 = Console.Read();
            value2 = Console.Read();
            Console.WriteLine(value1 + value2);
        }*/
    }
    class AssignmentOne   {
        static void Main(string[] args)
        {
            //1.adding two n-digit numbers using string array
            /*Console.WriteLine("Hello World!");
            ReadingNumbers addNumObj = new ReadingNumbers();
            addNumObj.AddNumbers();
            addNumObj.AddNums();*/

            //2.Adding two float binary digits
            /*AssignmentTwo assigntwo = new AssignmentTwo();
            assigntwo.addFloatBinary();

            //3.observing static variable scope
            //static variable
            Console.WriteLine(AssignmentTwo.count);
            AssignmentTwo assigntwo1 = new AssignmentTwo();
            assigntwo1.addFloatBinary();
            //count is not reset as it is static continues the previous count
            Console.WriteLine(AssignmentTwo.count);*/

            //4.Implementing is and as functionalities without using is and as keywords
            //isas isasobj = new isas();
            //isasobj.withoutIsAs();

            //5.Methods of object class
            //objectclass obcls = new objectclass();
            //obcls.ObjMethods();

            //6.out keyword in multithreading
            /*String res = "thread main";
            Thread t = new Thread(myfun(res));
            t.Start();
            Console.WriteLine("main thread");*/

            //7.Async and await for console application
            //asuncawait asaw = new asuncawait();

            //8.Automapper functionality using generics
            /*Automapper1<string, int> am1 = new Automapper1<string, int>();
            am1.Name = "Mapper1";
            am1.Age = 21;
            var config = new MapperConfiguration(cfg =>
                    cfg.CreateMap(typeof(Automapper1<,>), typeof(Automapper2<,>))
                );
            var mapper = new Mapper(config);
            var am2 = mapper.Map<Automapper1<string, int>, Automapper2<string, int>>(am1);
            Console.WriteLine(am2.Name + "," + am2.Age);*/

            //9.string class functionalities
            //stringClass sc = new stringClass();

        }
        /*static string myfun(out String res)
        {
            return("function thread");
        }*/
       
    }
}
