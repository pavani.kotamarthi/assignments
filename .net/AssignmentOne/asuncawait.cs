﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentOne
{
    class asuncawait
    {
        public asuncawait()
        {
            Console.WriteLine("async await");
            Method1();
        }
        public async Task Method1()
        {
            Console.WriteLine("Entered method1");
            string res = await Method2();
            Console.WriteLine("Method 2 completed," + res);
            Console.WriteLine("Method1 completed");
        }
        public async Task<string> Method2()
        {
            return "Breaked Method2";
        }
    }
}
