﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentOne
{
    class one{}
    class two:one{ }
    class three:two { }
    class isas
    {
        public isas()
        {
            Console.WriteLine("is as assignment");
        }
        public void withoutIsAs()
        {
            one o = new one();
            two t = new two();
            three th = new three();

            if (typeof(one).IsAssignableFrom(th.GetType()))
            {
                Console.WriteLine("is : true");
                Console.WriteLine((one)th);
            }
            else
            {
                Console.WriteLine("is : false");
                Console.WriteLine("as : null");
            }
        }
    }
}
