﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AssignmentOne
{
    class stringClass
    {
            public stringClass()
            {
                Console.WriteLine("String Class Functionality");

                string s1 = "hello";
                char[] ch = { 'c', 's', 'h', 'a', 'r', 'p' };
                string s2 = new string(ch);
                Console.WriteLine(s1);
                Console.WriteLine(s2);



                string s3 = "hello";
                Console.WriteLine(s1.Equals(s2));
                Console.WriteLine(s1.Equals(s3));



                Console.WriteLine("index of l in hello:" + s1.IndexOf('l'));
                Console.WriteLine("last index of l in hello:" + s1.LastIndexOf('l'));



                string s4 = s1.Replace("hello", "Hii");
                Console.WriteLine(s4 + " ," + s1);



                string s5 = s4.Replace('i', 'a');
                Console.WriteLine(s5);



                string s6 = "Hello, this is c# string assignment ";
                string s7 = s6.Remove(5);
                Console.WriteLine(s7 + " " + s6);



                string s8 = s6.Remove(5, 2);
                Console.WriteLine(s8);



                string[] s9 = s6.Split(' ');
                foreach (string s in s9)
                {
                    Console.WriteLine(s);
                }



                string s10 = string.Join("@", s9);
                Console.WriteLine(s10);



                string s11 = s10.Substring(10);
                Console.WriteLine(s11);



                string s12 = s10.Substring(10, 5);
                Console.WriteLine(s12);
            }
        }
    }

