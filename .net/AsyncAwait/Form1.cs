﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AsyncAwait
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public int clicked()
        {
            int seconds;
            Thread.Sleep(10000);
            seconds = 10;
            return seconds;
        }

        private async void button1_click(object sender, EventArgs e)
        {
            Task<int> task = new Task<int>(clicked);
            task.Start();
            label1.Text = "Clicked Me";
            int seconds = await task;
            label1.Text = "clicked me " + seconds.ToString() + " seconds ago";

        }
    }
}
