﻿
namespace loginform
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uname = new System.Windows.Forms.TextBox();
            this.pswd = new System.Windows.Forms.TextBox();
            this.SubmitBtn = new System.Windows.Forms.Button();
            this.RegisterBtn = new System.Windows.Forms.Button();
            this.forgotpswd = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(171, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(171, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "password:";
            // 
            // uname
            // 
            this.uname.Location = new System.Drawing.Point(294, 65);
            this.uname.Name = "uname";
            this.uname.Size = new System.Drawing.Size(150, 31);
            this.uname.TabIndex = 2;
            // 
            // pswd
            // 
            this.pswd.Location = new System.Drawing.Point(294, 140);
            this.pswd.Name = "pswd";
            this.pswd.Size = new System.Drawing.Size(150, 31);
            this.pswd.TabIndex = 3;
            // 
            // SubmitBtn
            // 
            this.SubmitBtn.Location = new System.Drawing.Point(171, 231);
            this.SubmitBtn.Name = "SubmitBtn";
            this.SubmitBtn.Size = new System.Drawing.Size(112, 34);
            this.SubmitBtn.TabIndex = 4;
            this.SubmitBtn.Text = "Login";
            this.SubmitBtn.UseVisualStyleBackColor = true;
            this.SubmitBtn.Click += new System.EventHandler(this.SubmitBtn_Click);
            // 
            // RegisterBtn
            // 
            this.RegisterBtn.Location = new System.Drawing.Point(332, 231);
            this.RegisterBtn.Name = "RegisterBtn";
            this.RegisterBtn.Size = new System.Drawing.Size(112, 34);
            this.RegisterBtn.TabIndex = 5;
            this.RegisterBtn.Text = "Register";
            this.RegisterBtn.UseVisualStyleBackColor = true;
            this.RegisterBtn.Click += new System.EventHandler(this.RegisterBtn_Click);
            // 
            // forgotpswd
            // 
            this.forgotpswd.AutoSize = true;
            this.forgotpswd.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.forgotpswd.Location = new System.Drawing.Point(171, 190);
            this.forgotpswd.Name = "forgotpswd";
            this.forgotpswd.Size = new System.Drawing.Size(154, 25);
            this.forgotpswd.TabIndex = 6;
            this.forgotpswd.Text = "Forgot Password?";
            this.forgotpswd.Click += new System.EventHandler(this.forgotpswd_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 340);
            this.Controls.Add(this.forgotpswd);
            this.Controls.Add(this.RegisterBtn);
            this.Controls.Add(this.SubmitBtn);
            this.Controls.Add(this.pswd);
            this.Controls.Add(this.uname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox uname;
        private System.Windows.Forms.TextBox pswd;
        private System.Windows.Forms.Button SubmitBtn;
        private System.Windows.Forms.Button RegisterBtn;
        private System.Windows.Forms.Label forgotpswd;
    }
}

