﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace loginform
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SubmitBtn_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\pavani.kotamarthi\Documents\userpswd.mdf;Integrated Security=True;Connect Timeout=30");
            connection.Open();
            SqlCommand scommand = new SqlCommand("select * from LoginDetails where username='" + uname.Text + "' and password='" + pswd.Text + "'",connection);
            
            SqlDataReader sdr = scommand.ExecuteReader();
            if (sdr.Read())
            {
                MessageBox.Show("Login sucess");
            }
            else
            {
                MessageBox.Show("Incorrect");
            }
            connection.Close();
            
        }

        private void RegisterBtn_Click(object sender, EventArgs e)
        {
            
            /*connection.Open();
            SqlCommand scommand = new SqlCommand("insert into LoginDetails(username,password) values('"+uname.Text+"','"+pswd.Text+"')", connection);
            int status = scommand.ExecuteNonQuery();
            if (status.ToString()=="1")
            {
                MessageBox.Show("Registered successfully");
            }
            else
            {
                MessageBox.Show("Registration failed");
            }
            connection.Close();*/
            //code to avoid sql injection we use another method with parameters
            RegisterDetails(uname.Text, pswd.Text);
        }
        private void RegisterDetails(string uname, string pwd)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\pavani.kotamarthi\Documents\userpswd.mdf;Integrated Security=True;Connect Timeout=30");
            string query = "insert into LoginDetails values(@uname,@pwd)";
            connection.Open();
            SqlCommand sc = new SqlCommand(query, connection);
            sc.Parameters.AddWithValue("@uname", uname);
            sc.Parameters.AddWithValue("@pwd", pwd);
            int status = Convert.ToInt32(sc.ExecuteNonQuery());
            if (status == 1)
            {
                MessageBox.Show("Registered Successfully");
            }
            else
            {
                MessageBox.Show("Registeration failed");
            }
            connection.Close();
        }
        private void forgotpswd_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\pavani.kotamarthi\Documents\userpswd.mdf;Integrated Security=True;Connect Timeout=30");
            connection.Open();
            //SqlCommand scommand = new SqlCommand("update LoginDetails set password='"+pswd.Text+"' where username='"+uname.Text+"'", connection);
            //string query = "update Login set password='"+PasswordField.Text+"' where username='"+UserNameField.Text+"';";
           
            SqlCommand scommand = new SqlCommand("UpdateData", connection)
            {
                CommandType = CommandType.StoredProcedure
            };
            SqlParameter usernameParam = new SqlParameter
            {
                ParameterName = "@username", //Parameter name defined in stored procedure
                SqlDbType = SqlDbType.VarChar, //Data Type of Parameter
                Value = uname.Text,
                Direction = ParameterDirection.Input //Specify the parameter as input
            };
            //add the parameter to the SqlCommand object
            scommand.Parameters.Add(usernameParam);
            scommand.Parameters.AddWithValue("@password", pswd.Text);
            int status = scommand.ExecuteNonQuery();
            if (status.ToString() == "1")
            {
                MessageBox.Show("password updated successfully");
            }
            else
            {
                MessageBox.Show("password updation failed. Please register");
            }
            connection.Close();
        }
    }
}
