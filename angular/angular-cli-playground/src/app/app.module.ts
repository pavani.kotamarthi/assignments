import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SampCompComponent } from './samp-comp/samp-comp.component';
import { ParentComponent } from './parent/parent.component';
import { ChildComponent } from './child/child.component';
import { SampModuleModule } from './samp-module/samp-module.module';
import { Samp1Module } from './samp1/samp1.module';
import { Samp2Module } from './samp2/samp2.module';
import { customPipe } from './samp-comp/custpipe.pipe';
import {
  FormControl,
  FormsModule,
  NgControl,
  ReactiveFormsModule,
} from '@angular/forms';
import { CustdirDirective } from './custdir.directive';
import { CustserviceService } from './custservice.service';
import { RxjscompComponent } from './rxjscomp/rxjscomp.component';
import { Comp1Component } from './practice-comps/comp1/comp1.component';
import { Comp2Component } from './practice-comps/comp1/comp2/comp2.component';

@NgModule({
  declarations: [
    AppComponent,
    SampCompComponent,
    ParentComponent,
    ChildComponent,
    customPipe,
    CustdirDirective,
    RxjscompComponent,
    Comp1Component,
    Comp2Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SampModuleModule,
    Samp1Module,
    Samp2Module,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [CustserviceService],
  bootstrap: [AppComponent],
})
export class AppModule {}
