import { Component, Input, OnInit, Output, EventEmitter,SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: 'child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  //Receiving data from parent
  @Input() childMessage:string;
  //sharing data via @Output() through events
  @Output() public childEvent = new EventEmitter();
  message = "pavani";
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void{
    console.log('changed in child '+changes);
    // console.log('changed ' + changes.value)
  }
  ngOnInit(): void {
    console.log('initialised child')
  }
  ngDoCheck(){
    console.log('do check child');
  }
  ngAfterContentInit(){
    console.log('child content initialised')
  }
  ngAfterContentChecked(){
    console.log('child content check')
  }
  ngAfterViewInit(){
    console.log('child view intialised')
  }
  ngAfterViewChecked(){
    console.log('child view checked')
  }
  ngOnDestroy(){
    alert('child component destroyed')
  }
  sendMsg(){
    this.childEvent.emit('hello its a msg from child');
  }
}
