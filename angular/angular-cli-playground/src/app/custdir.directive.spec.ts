import { CustdirDirective } from './custdir.directive';

describe('CustdirDirective', () => {
  it('should create an instance', () => {
    let elRefMock = {
      nativeElement: document.createElement('p')
    };
    const directive = new CustdirDirective(elRefMock);
    expect(directive).toBeTruthy();
  });
});
