import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCustdir]'
})
export class CustdirDirective {

  constructor(private ele:ElementRef) { 
    ele.nativeElement.style.color= 'green';
    ele.nativeElement.style.backgroundColor = 'yellow';
    ele.nativeElement.innerHTML += 'Hello it worked! '
  }

}
