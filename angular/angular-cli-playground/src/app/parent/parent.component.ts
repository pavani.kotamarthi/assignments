import { AfterViewInit, Component, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: 'parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit,AfterViewInit,OnChanges {
  
  public parentMessage = "msg from Parent";
  public msg:string;
  
  constructor() { }
  msg1:string;
  @ViewChild(ChildComponent) child;
  ngOnChanges(changes:SimpleChanges){
    console.log("parent changes");
  }
  ngOnInit(): void {
    console.log('Initialised parent')
  }
  ngDoCheck(){
    console.log('Do check in parent')
  }
  ngAfterContentInit(){
    console.log('parent content initialised')
  }
  ngAfterContentChecked(){
    console.log('content check in parent');
  }
  ngAfterViewInit(){
    console.log('view init in parent',this.child.message)
  }
  ngAfterViewChecked(){
    console.log('view check in parent')
  }
  ngOnDestroy(){
    alert('parent component destroyed')
  }
}
