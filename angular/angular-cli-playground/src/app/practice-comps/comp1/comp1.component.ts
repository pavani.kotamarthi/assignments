import {
  Component,
  forwardRef,
  OnInit,
  Optional,
  ViewEncapsulation,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NgControl,
  NG_VALUE_ACCESSOR,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => Comp1Component),
      multi: true,
    },
  ],
})
export class Comp1Component implements ControlValueAccessor, OnInit {
  constructor(@Optional() public ngControl: NgControl) {
    if (ngControl) {
      ngControl.valueAccessor = this;
    }
  }

  template = true;
  arraydisplay = true;
  array = [1, 2, 3, 4];

  input = new FormControl('input', [
    Validators.required,
    Validators.minLength(8),
  ]);
  inputValue;
  ngOnInit() {
    console.log(this.ngControl);
  }

  onTouched = () => {};
  onChange = (_) => {};
  registerOnChange(fn: any) {
    this.onChange = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }
  writeValue(value) {
    this.inputValue = value;
  }
  inputOnChange(event) {
    this.inputValue = event.target.value;
    this.onChange(this.inputValue);
  }
}
