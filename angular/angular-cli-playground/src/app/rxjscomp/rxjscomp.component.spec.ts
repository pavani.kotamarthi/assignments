import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RxjscompComponent } from './rxjscomp.component';

describe('RxjscompComponent', () => {
  let component: RxjscompComponent;
  let fixture: ComponentFixture<RxjscompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RxjscompComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RxjscompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
