import { Component, OnInit } from '@angular/core';
import {Observable, Subject, BehaviorSubject, ReplaySubject, AsyncSubject} from 'rxjs';

@Component({
  selector: 'rxjscomp',
  templateUrl: './rxjscomp.component.html',
  styleUrls: ['./rxjscomp.component.css']
})
export class RxjscompComponent implements OnInit {
  status: unknown;

  constructor() { }

  ngOnInit(): void {
    // observable creation
    const obs = new Observable(observer => {
      setTimeout(()=>observer.next('Observer first msg'),3000);
    });
    // creating observer
    const observer = {
      next: value => console.log('Observer got the next value:' + value),
      error: error => console.log('Observer got an error' + error),
      complete:() => console.log('Observer got a complete notification'),
    };
    // Subscription
    obs.subscribe(observer);
    // unsubscription
    // const secondsCounter = interval(1000);
    // const subscription = secondsCounter.subscribe(n =>
    // console.log(`It's been ${n + 1} seconds since subscribing!`));
    // setTimeout(()=>{
    //   subscription.unsubscribe();
    //   console.log('successfully unsubscribed')},10000)

    // subject
    const sub = new Subject();
    sub.subscribe({
      next: (value) => console.log('subject First observer: ' + value)
    });
    sub.subscribe({
      next: (value) => console.log('subject Second observer: ' + value)
    });
    sub.next("sub msg1");
    sub.next("sub msg2");

    // BehaviorSubject
    const bsub = new BehaviorSubject('bsub msg');
    bsub.subscribe({
      next: (value) => console.log('Behaviour sub First observer: ' + value)
    });
    bsub.next('bsub msg1');
    bsub.subscribe({
        next: (value) => console.log('Behaviour sub Second observer:' + value)
    });
    bsub.next('bsub msg2');

    // ReplaySubject
    const rSub = new ReplaySubject(2);
    rSub.subscribe({
        next: (value) => console.log('replay sub First observer:' + value)
    });
    rSub.next('rsub msg1');
    rSub.next('rsub msg2');
    rSub.next('rsub msg3');
    rSub.subscribe({
        next: (value) => console.log('replay sub Second observer:' + value)
    });
    rSub.next('rsub msg4');

    // AsyncSubject
    const asub = new AsyncSubject();
    asub.subscribe({
        next: (value) => console.log('async sub First observer:' + value)
    });
    asub.next('asub msg1');
    asub.next('asub msg2');
    asub.complete();
    asub.subscribe({
        next: (value) => console.log('async sub Second observer:' + value)
    });
    asub.next('asub msg3');

    new Observable(observer=>
      {
        setTimeout(()=>{observer.next("In progress")},2000)
        setTimeout(()=>{observer.next("In processing")},4000)
        setTimeout(()=>{observer.next("completed")},6000)
        // setTimeout(()=>{observer.error()},2000)
        setTimeout(()=>{observer.complete()},2000)
        // it doesn't show
        setTimeout(()=>{observer.next("after complete")},8000)
      }).subscribe(val=>this.status=val);
      
  }
  
}
