import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SampCompComponent } from './samp-comp.component';

describe('SampCompComponent', () => {
  let component: SampCompComponent;
  let fixture: ComponentFixture<SampCompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SampCompComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SampCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
