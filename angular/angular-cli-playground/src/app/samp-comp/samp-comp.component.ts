import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { CustserviceService } from '../custservice.service';
import { ParentComponent } from '../parent/parent.component';
import { FormControl, FormGroup, Validators, FormArray, AbstractControl, ValidatorFn, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-samp-comp',
  templateUrl: './samp-comp.component.html',
  styleUrls: ['./samp-comp.component.css'],
  providers:[CustserviceService]
})

export class SampCompComponent implements OnInit {
  actionName = "pavani";
  className = ["btn1","btn2"];//Binding multiple classes can be done by arrays, objects with classnames as key : values as truthy or falsy and strings with space delimiter
  width = "100px";
  styles = "width:50px;height:100px;border:2px solid red;color:green";// here width is overriden by the previous width
  birthday = new Date(1999,9,23);
  gender = "female";
  person_name="pavani"
  currentClass = "true";
  public my_name = "durgapavani";

  currentclass1 = {};
  
  Currentclass1(){
    this.currentclass1={
      style1:true,
      style2:false
    }
    return this.currentclass1;
  }
  currentstyle = {}
  Currentstyle(){
    this.currentstyle = {
      'font-style': this.currentclass1?'italic':'normal',
      'font-size':!this.currentclass1?'10px':'20px'
    }
    return this.currentstyle;
  }

  array_items = [1,2,3,4,5,6];
  // Dynamic component
  @ViewChild("alertContainer", { read: ViewContainerRef }) container;
 componentRef: ComponentRef<ParentComponent>;
 
  constructor(private resolver: ComponentFactoryResolver,private _nameservice:CustserviceService,private fb:FormBuilder) {}
  
  createComponent(type:string) {
    this.container.clear();
    const factory: ComponentFactory<ParentComponent> = this.resolver.resolveComponentFactory(ParentComponent);

    this.componentRef = this.container.createComponent(factory);
  }
  ngOnDestroy() {
    this.componentRef.destroy();    
  }

  pipeoperation(val:number){
    this.array_items.push(val)
    console.log(this.array_items)
  }
  names = [];
  ngOnInit(): void {
    // services
    this.names = this._nameservice.Names;
    //subscription to forms
    this.fg.get('email').valueChanges.subscribe(val=>{
      console.log('email changed');
      console.log('changed value',val);
    });
  }

  // Reactive form
  fg:FormGroup = new FormGroup({
    fc1: new FormControl('',this.color()),
    email: new FormControl('',{validators:[Validators.required,Validators.email],updateOn:'blur'}),
    fc2: new FormControl('',{validators:[Validators.required,Validators.minLength(4),Validators.maxLength(12)]}),
    skills: new FormArray([])
  });
  get skills(){
    return this.fg.get('skills') as FormArray;
  }
  addSkills(){
    this.skills.push(new FormControl(''));
  }
  fcc = new FormControl('Hello');
  inputValue:any;
  setDefaultValue(){
    this.fcc.setValue('First Input');
  }
  getValue(){
    this.inputValue = this.fcc.value;
  }
  onsubmit()
  {
    console.log(this.fg.value);
  }
  // TemplateDriven form
  inputTaken = '';
  
  arr = new FormArray([
    new FormControl(),
    new FormControl()
  ]);
  print(){
    console.log(this.arr.value);
    this.arr.setValue(['durga','pavani']);
    console.log(this.arr.value);
  }
  // custom validator
  color():ValidatorFn{
    return (control:AbstractControl) : { [key: string]: any } | null =>  
    control.value?.toLowerCase() === 'orange' 
        ? null : {wrongColor: control.value};
  }
}
