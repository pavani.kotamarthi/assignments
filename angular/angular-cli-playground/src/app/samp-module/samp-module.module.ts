import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FCompComponent } from 'src/app/f-comp/f-comp.component';
import { Samp1Module } from '../samp1/samp1.module';

@NgModule({
  declarations: [ 
    FCompComponent
  ],
  imports: [
    CommonModule,
    Samp1Module,
  ],
  exports:[FCompComponent]
})
export class SampModuleModule { }
