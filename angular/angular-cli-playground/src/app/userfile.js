"use strict";
exports.__esModule = true;
var rxjs_1 = require("rxjs");
// observable creation
var myObs = new rxjs_1.Observable(function (observer) {
    setTimeout(function () { return observer.next('Observer first msg'); }, 3000);
});
// observers
var observer = {
    next: function (value) { return console.log('Observer got the next value:' + value); },
    error: function (error) { return console.log('Observer got an error' + error); },
    complete: function () { return console.log('Observer got a complete notification'); }
};
// subscription
myObs.subscribe(observer);
// Subject
var mySub = new rxjs_1.Subject();
mySub.subscribe({
    next: function (value) { return console.log('First observer:' + value); }
});
mySub.subscribe({
    next: function (value) { return console.log('Second observer:' + value); }
});
mySub.next('msg 1');
mySub.next('msg 2');
// BehaviorSubject
var myBsub = new rxjs_1.BehaviorSubject('Bsubj msg');
myBsub.subscribe({
    next: function (value) { return console.log('First observer:' + value); }
});
myBsub.next('bsub msg1');
myBsub.subscribe({
    next: function (value) { return console.log('Second observer:' + value); }
});
myBsub.next('bsub msg2');
// ReplaySubject
var repSub = new rxjs_1.ReplaySubject(2);
repSub.subscribe({
    next: function (value) { return console.log('First observer:' + value); }
});
repSub.next('rsub msg1');
repSub.next('rsub msg2');
repSub.next('rsub msg3');
repSub.subscribe({
    next: function (value) { return console.log('Second observer:' + value); }
});
repSub.next('rsub msg4');
// AsyncSubject
var asub = new rxjs_1.AsyncSubject();
asub.subscribe({
    next: function (value) { return console.log('First observer:' + value); }
});
asub.next('asub msg1');
asub.next('asub msg2');
asub.complete();
asub.subscribe({
    next: function (value) { return console.log('Second observer:' + value); }
});
asub.next('asub msg3');
