import {Observable, Subject, BehaviorSubject, ReplaySubject, AsyncSubject} from 'rxjs';

// observable creation
const myObs = new Observable(observer=>{
    setTimeout(()=>observer.next('Observer first msg'),3000);
})

// observers
const observer = {
    next: value => console.log('Observer got the next value:' + value),
    error: error => console.log('Observer got an error' + error),
    complete:() => console.log('Observer got a complete notification'),
  };
// subscription
myObs.subscribe(observer);

// Subject
const mySub = new Subject();
mySub.subscribe({
    next: (value) => console.log('First observer:' + value)
});
mySub.subscribe({
    next: (value) => console.log('Second observer:' + value)
});
mySub.next('msg 1');
mySub.next('msg 2');

// BehaviorSubject
const myBsub = new BehaviorSubject('Bsubj msg');
myBsub.subscribe({
    next: (value) => console.log('First observer:' + value)
});
myBsub.next('bsub msg1');
myBsub.subscribe({
    next: (value) => console.log('Second observer:' + value)
});
myBsub.next('bsub msg2');

// ReplaySubject
const repSub = new ReplaySubject(2);
repSub.subscribe({
    next: (value) => console.log('First observer:' + value)
});
repSub.next('rsub msg1');
repSub.next('rsub msg2');
repSub.next('rsub msg3');
repSub.subscribe({
    next: (value) => console.log('Second observer:' + value)
});
repSub.next('rsub msg4');

// AsyncSubject
const asub = new AsyncSubject();
asub.subscribe({
    next: (value) => console.log('First observer:' + value)
});
asub.next('asub msg1');
asub.next('asub msg2');
asub.complete();
asub.subscribe({
    next: (value) => console.log('Second observer:' + value)
});
asub.next('asub msg3');
