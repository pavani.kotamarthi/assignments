import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { StoreModule } from '@ngrx/store';
import { counterReducer } from './counter.reducer';
import { MycounterComponent } from './mycounter/mycounter.component';
import { PostsComponent } from './posts/posts.component';
import { DetailscompComponent } from './detailscomp/detailscomp.component';

@NgModule({
  declarations: [
    AppComponent,
    MycounterComponent,
    PostsComponent,
    DetailscompComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({count:counterReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
