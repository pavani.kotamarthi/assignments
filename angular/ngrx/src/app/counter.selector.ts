import { createFeatureSelector, createSelector } from "@ngrx/store";
import { CounterState } from "./counter.state";


const  getCounterState = createFeatureSelector<CounterState>('count');

export const getName = createSelector(getCounterState,state =>{
    return state.name;
})