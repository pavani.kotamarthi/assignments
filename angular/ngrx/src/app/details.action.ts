import { createAction,props } from "@ngrx/store";

export const details = createAction('[Details Component] Details',
                                     props<{ username: string; password: string }>());