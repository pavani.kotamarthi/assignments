import { Action,on,createReducer } from "@ngrx/store";
import { details } from "./details.action";

export const initialState = '';

const _counterReducer = createReducer(
    initialState,
    on(details, (state)=>state)
);

export function  counterReducer(state: any,action: any) {
    return _counterReducer(state,action);
}