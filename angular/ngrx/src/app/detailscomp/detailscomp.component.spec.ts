import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailscompComponent } from './detailscomp.component';

describe('DetailscompComponent', () => {
  let component: DetailscompComponent;
  let fixture: ComponentFixture<DetailscompComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailscompComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailscompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
