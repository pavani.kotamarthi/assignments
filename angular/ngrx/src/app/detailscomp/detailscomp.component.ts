import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { details } from "../details.action";

@Component({
  selector: 'app-detailscomp',
  templateUrl: './detailscomp.component.html',
  styleUrls: ['./detailscomp.component.css']
})
export class DetailscompComponent implements OnInit {

  detail:string | undefined;
  constructor(private store:Store<{details:string}>) { }

  ngOnInit(): void {
  }

  details(username:string ,password:string){
    this.store.dispatch(details({username: username,password: password}));
  }

}
