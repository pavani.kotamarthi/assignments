import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { increment, decrement, reset } from '../counter.action';
import { getName } from '../counter.selector';

@Component({
  selector: 'mycounter',
  templateUrl: './mycounter.component.html',
  styleUrls: ['./mycounter.component.css']
})
export class MycounterComponent implements OnInit {
  count$ : Observable<number>;
  name: Observable<string>;
  constructor(private store: Store<{ count: number }>) { 
    this.count$ = store.select('count');
    // this.count$ = store.select(getCounter)
    //  this.my_name = this.store.select(getName)/*.subscribe((data)=>{*/
    //   console.log('name called');
    //   this.my_name = data
    // });
    // this.store.select(getName).subscribe((data)=>{
    //   this.name = data;
    //   console.log(data)
    // })
    this.name = this.store.select(getName);
    
  }

  ngOnInit(): void {
    console.log(this.name)
  }
  increment() {
    this.store.dispatch(increment());
  }
 
  decrement() {
    this.store.dispatch(decrement());
  }
 
  reset() {
    this.store.dispatch(reset());
  }

}
