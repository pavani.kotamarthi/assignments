import { state } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts:Observable<[]> = this.store.select(state => state.posts);

  constructor(private store: Store<{ posts: [] }>) { }

  ngOnInit(): void {
    this.store.dispatch({ type: '[]' });
  }

}
