import { Component, OnInit } from '@angular/core';
import { FormControl,FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  fg:FormGroup = new FormGroup({
    username:new FormControl('',{validators:[Validators.required,Validators.minLength(4)],updateOn:'blur'}),
    password:new FormControl('',{validators:[Validators.required,Validators.minLength(8)],updateOn:'blur'})
  });
  subform=false;
  submitform(){
    console.log('submitted form')
    this.subform=true;
  }

  
  // onload = ()=>{
  //   window.addEventListener("beforeunload",function(e) {
  //     if(subform){
  //       return undefined;
  //     }
  //     e.returnValue='';
  //   })
  // }
  constructor() { 
    // window.onload = () =>{
    //     confirm("Data will be lost if you leave the page, are you sure?");
    // };
    // addEventListener('beforeunload',()=>{
    //   return confirm('Confirmed??')
    // })
    window.addEventListener("beforeunload", function(event) {
      event.returnValue=''
    }) 
  }
  
  ngOnInit(): void {
  }
  // fn() {
  //   if(this.subform){
  //     console.log('form submitted')
  //     return undefined;
  //   }
  //   else{
  //     console.log('form unsubmitted')
  //     window.addEventListener("beforeunload", function(event) {
  //       event.returnValue=''
  //     }) 
  //   }
  // }
}
