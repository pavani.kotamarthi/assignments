import { NgModule } from '@angular/core';

import { RouterModule, Routes, /*PreloadAllModules*/} from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { Adminchild1Component } from './adminchild1/adminchild1.component';
import { Adminchild2Component } from './adminchild2/adminchild2.component';

import { AdminguardGuard } from './adminguard.guard';
import { DefaultcompComponent } from './defaultcomp/defaultcomp.component';
import { EmplistComponent } from './emplist/emplist.component';
import { FirstGuardGuard } from './first-guard.guard';

import { FirstcompComponent } from './firstcomp/firstcomp.component';
import { PagenotfoundcompComponent } from './pagenotfoundcomp/pagenotfoundcomp.component';
import { SecondcompComponent } from './secondcomp/secondcomp.component';

const routes: Routes = [
  {path:'first',component:FirstcompComponent,canActivate:[FirstGuardGuard]},
  {path:'second',component:SecondcompComponent,resolve:{
    data:AdminguardGuard
  }},
  {path:'emplist/:empid',component:EmplistComponent},
  {path:'lazymod',loadChildren:()=>import('./lazymod/lazymod.module').then(m=>m.LazymodModule),
  canLoad:[AdminguardGuard],},
  {path:'admin',
  canActivate:[AdminguardGuard],
  canActivateChild:[AdminguardGuard],
  children:[
    {path:'',component:AdminComponent,canDeactivate:[AdminguardGuard]},
    {path:'adminchild1',component:Adminchild1Component},
    {path:'adminchild2',component:Adminchild2Component}
  ]
},
  { path: '',   redirectTo: '/first', pathMatch:'full' },
  {path:'default',component:DefaultcompComponent},
  {path:'**',component:PagenotfoundcompComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,/*{preloadingStrategy:PreloadAllModules}*/)],
  exports: [RouterModule]
})
export class AppRoutingModule { 
}
export const routingcomponents = [FirstcompComponent,SecondcompComponent,EmplistComponent]
