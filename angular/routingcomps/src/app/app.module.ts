import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule,routingcomponents } from './app-routing.module';

import { AppComponent } from './app.component';
import { PagenotfoundcompComponent } from './pagenotfoundcomp/pagenotfoundcomp.component';
import { AdminComponent } from './admin/admin.component';
import { Adminchild1Component } from './adminchild1/adminchild1.component';
import { Adminchild2Component } from './adminchild2/adminchild2.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DefaultcompComponent } from './defaultcomp/defaultcomp.component';
import { RouterTestingModule } from '@angular/router/testing';

console.log('module loaded');

@NgModule({
  declarations: [
    AppComponent,
    routingcomponents,
    PagenotfoundcompComponent,
    AdminComponent,
    Adminchild1Component,
    Adminchild2Component,
    DefaultcompComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterTestingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
