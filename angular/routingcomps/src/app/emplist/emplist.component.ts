import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap,Router } from "@angular/router";

@Component({
  selector: 'app-emplist',
  templateUrl: './emplist.component.html',
  styleUrls: ['./emplist.component.css']
})
export class EmplistComponent implements OnInit {

  public employeeId;
  constructor(private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
    // let id = parseInt(this.route.snapshot.paramMap.get('empid'));
    // this.employeeId=id;
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id = parseInt(params.get('empid'));
      this.employeeId=id;
    })
  }
  goPrevious(){
    let prev = this.employeeId - 1;
    this.router.navigate(['/emplist',prev])
  }
  goNext(){
    let next = this.employeeId + 1;
    this.router.navigate(['/emplist',next])
  }
  gotoemplist(){
    let selectedempid = this.employeeId ? this.employeeId : null;
    this.router.navigate(['/first',{empid:selectedempid}])
    // this.router.navigate(['../',{empid:selectedempid}],{relativeTo:this.route})
    // console.log(history.length);
    // if(history.length<=2){
    //   history.go(-1);
    // }
    // else{
    //   this.router.navigate(['/first']);
    // }
  }
}
