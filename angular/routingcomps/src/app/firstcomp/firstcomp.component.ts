import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute, ParamMap } from '@angular/router';
import { PlatformLocation } from '@angular/common';

@Component({
  selector: 'app-firstcomp',
  templateUrl: './firstcomp.component.html',
  styleUrls: ['./firstcomp.component.css']
})
export class FirstcompComponent implements OnInit {
  employees = [
    {"empid":1,"name":"x"},
    {"empid":2,"name":"y"},
    {"empid":3,"name":"z"}
  ]
  public selectedempId;
  constructor(private router:Router,private route:ActivatedRoute,/*private location:PlatformLocation*/) { 
    // this.location.onPopState(()=>{
    //   console.log('pressed back');
    //   this.router.navigateByUrl('/first');
    //   history.forward();
    // history.back(); //popstate event triggers only when there is a back or forward of the history
    // history.go();
    // })
    // history.pushState(null,null,'/first');
    //  history.back();
    // this.location.forward();//steps upto single page back
    // this.location.pushState(null,null,'/first');
    // this.location.replaceState(null,null,'/first');
  }
  ngOnInit(): void {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      console.log('subscribed');
      let id = parseInt(params.get('empid'));
      this.selectedempId=id;
    })
  }
  selectemp(emps){
    this.router.navigate(['/emplist',emps.empid]);
    // this.router.navigate([emps.empid],{relativeTo:this.route});
  }
  isSelected(emps){
    return emps.empid === this.selectedempId;
  }
  godefault(){
    // (history.length==1 || history.length==0)?this.router.navigate(['/first']):history.go(-1);
    // var x = history.length;
    //   if(x>2){
    //     history.go(-1);
    //     x--;
    //   }
    //   else{
        this.router.navigate(['/first']);
      // }
  }
}
