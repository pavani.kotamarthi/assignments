import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.css']
})
export class Comp2Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  gotoemplist(){
    var x=history.length;
    if(x>2){
      history.go(-1);
      x--;
    }
    else{
      console.log('less than one');
      this.router.navigate(['/first']);
    }
  }

}
