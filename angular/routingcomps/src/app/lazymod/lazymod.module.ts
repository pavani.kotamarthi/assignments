import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazymodRoutingModule } from './lazymod-routing.module';
import { Comp1Component } from './comp1/comp1.component';
import { Comp2Component } from './comp2/comp2.component';

console.log('lazy module loaded!!');
@NgModule({
  declarations: [
    Comp1Component,
    Comp2Component
  ],
  imports: [
    CommonModule,
    LazymodRoutingModule
  ]
})
export class LazymodModule { }
