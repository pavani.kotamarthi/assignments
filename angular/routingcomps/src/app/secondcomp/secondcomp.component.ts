import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-secondcomp',
  templateUrl: './secondcomp.component.html',
  styleUrls: ['./secondcomp.component.css']
})
export class SecondcompComponent implements OnInit {
  public employeeId;
  constructor(private route:ActivatedRoute,private router:Router,private activatedroute:ActivatedRoute) { }
  ngOnInit(): void {
    console.log(this.activatedroute.snapshot.data);
  }
  gotoemplist(){
    var x=history.length;
    if(x>2){
      history.go(-1);
      x--;
    }
    else{
      console.log('less than one');
      this.router.navigate(['/first']);
    }
  }

}
