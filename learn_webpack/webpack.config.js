const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require('path');

module.exports = {
    mode: 'development',
    entry:{
        output_file: path.resolve(__dirname, './sample.js'),
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'deploy')
    },
    plugins: [
        new HtmlWebpackPlugin({
        title: "Webpack Output",
        }),
    ],
};