///<reference path='./nsfile.ts'/>
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
// import a_decl from "./decfile";
// console.log(a_decl);
// import {Addition} from './secondprog'; 
console.log("hello");
function fun(str) {
    return "Hello " + str;
}
var x = fun("first ts program");
console.log(x);
var floats = 10.11;
console.log(floats);
var str1 = "pavani";
console.log(str1);
var arr = [1, 2, 3, 4];
console.log(arr);
var arr1 = ["pav", "ani", "xyz"];
console.log(arr1);
var mul_arr = [[1, 2], [3, 4]];
console.log(mul_arr);
var copy_arr1 = __spreadArray([], arr);
var copy_arr2 = __spreadArray(__spreadArray([], arr), [5, 6]);
var merge_arr = __spreadArray(__spreadArray([], arr), arr1);
console.log(copy_arr1);
console.log(copy_arr2);
console.log(merge_arr);
var arr_obj = new Array("xd", "yd", "zd");
console.log(arr_obj);
var tup = ["pavani", 1, "techigai", 2];
console.log(tup[0] + " working in " + tup[2]);
var tup1 = ["pavani", 1, "techigai", 2];
console.log(tup1);
var stats;
(function (stats) {
    stats[stats["sunday"] = 1] = "sunday";
    stats[stats["monday"] = 2] = "monday";
    stats["tuesday"] = "tue";
})(stats || (stats = {}));
console.log(stats.monday);
console.log(stats.tuesday);
var samp_class = /** @class */ (function () {
    function samp_class(name, id) {
        this.name = name;
        this.id = id;
    }
    samp_class.prototype.display = function () {
        console.log("name " + this.name + " and id " + this.id);
    };
    return samp_class;
}());
var samp_obj = new samp_class("pavani", 116);
samp_obj.display();
var int_obj = {};
int_obj.name = "pavani";
int_obj.company = "techigai";
console.log(int_obj.name, int_obj.company);
var cls = /** @class */ (function () {
    function cls() {
        this.name = "pavani";
        console.log(this.name + " class constructor invoked");
    }
    cls.prototype.display = function (name) {
        return this.name + " working in techigai";
    };
    return cls;
}());
// var obj_ifc = <infc>{};
// console.log(obj_ifc.display);
// console.log(obj_ifc.name);
// let details_obj1 =new first_np.fc("pavani");
// console.log(details_obj1.display());
// let addObject = new Addition(10, 20);   
// addObject.Sum();
var it_arr = [1, 2, 3, 4];
for (var _i = 0, it_arr_1 = it_arr; _i < it_arr_1.length; _i++) {
    var item = it_arr_1[_i];
    console.log(item);
}
// function * my_iterator(){
//     yield 1,
//     yield 2
// }
// const iter = my_iterator();
// console.log(iter.next())
// user-defined iterables
// const myIterable = {
//     *[Symbol.iterator]() {
//         yield 1;
//         yield 2;
//         yield 3;
//     }
// }
// for (let value of myIterable) {
//     console.log(value);
// }
