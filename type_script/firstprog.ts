///<reference path='./nsfile.ts'/>

// import a_decl from "./decfile";
// console.log(a_decl);

// import {Addition} from './secondprog'; 
console.log("hello");
function fun(str: String){
    return `Hello ${str}`;
}
let x = fun("first ts program");
console.log(x);
let floats: number = 10.11;
console.log(floats);
let str1: string = "pavani";
console.log(str1);
let arr:number[] = [1,2,3,4];
console.log(arr);
let arr1:Array<string> = ["pav","ani","xyz"];
console.log(arr1);
let mul_arr:number[][] = [[1,2],[3,4]];
console.log(mul_arr);
let copy_arr1 = [...arr];
let copy_arr2 = [...arr,5,6];
let merge_arr = [...arr,...arr1];
console.log(copy_arr1);
console.log(copy_arr2);
console.log(merge_arr);
let arr_obj = new Array("xd","yd","zd");
console.log(arr_obj);
let tup = ["pavani",1,"techigai",2];
console.log(`${tup[0]} working in ${tup[2]}`);
let tup1:[string,number,string,number]=["pavani",1,"techigai",2]
console.log(tup1)
enum stats {
    sunday=1,
    monday,
    tuesday='tue'
}
console.log(stats.monday);
console.log(stats.tuesday);
class samp_class{
    name:string;
    id:number;
    constructor(name:string,id:number){
        this.name=name;
        this.id = id
    }
    display():void{
        console.log(`name ${this.name} and id ${this.id}`);
    }
}
let samp_obj = new samp_class("pavani",116);
samp_obj.display();
interface samp_interface{
    company:string;
}
interface samp_interface1 extends samp_interface{
    name:string;
}
let int_obj = <samp_interface1>{};
int_obj.name="pavani";
int_obj.company="techigai"
console.log(int_obj.name,int_obj.company);
class cls{
    name:string="pavani";
    constructor(){
        console.log(`${this.name} class constructor invoked`);
    }
    display(name:string):string{
        return `${this.name} working in techigai`;
    }
}
interface infc extends cls{
    display1(name:string):string;
}
//  class cls1 implements infc{
//      display1(name:string):string
//      {
//          return name
//      }
//      display(name:string):string{
//          return name
//      }
//  }
interface infc1 extends infc{
    
}
// var obj_ifc = <infc>{};
// console.log(obj_ifc.display);
// console.log(obj_ifc.name);


// let details_obj1 =new first_np.fc("pavani");
// console.log(details_obj1.display());

 
  
// let addObject = new Addition(10, 20);   

// addObject.Sum();

const it_arr = [1,2,3,4];
for(let item of it_arr){
    console.log(item);
}

// function * my_iterator(){
//     yield 1,
//     yield 2
// }
// const iter = my_iterator();
// console.log(iter.next())

// user-defined iterables
// const myIterable = {
//     *[Symbol.iterator]() {
//         yield 1;
//         yield 2;
//         yield 3;
//     }
// }

// for (let value of myIterable) {
//     console.log(value);
// }

