var first_np;
(function (first_np) {
    var fc = /** @class */ (function () {
        function fc(nam) {
            this.nam = nam;
        }
        fc.prototype.display = function () {
            return this.nam + " is the name";
        };
        return fc;
    }());
    first_np.fc = fc;
})(first_np || (first_np = {}));
