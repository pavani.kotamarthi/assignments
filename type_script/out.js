var first_np;
(function (first_np) {
    class fc {
        constructor(nam) {
            this.nam = nam;
        }
        display() {
            return `${this.nam} is the name`;
        }
    }
    first_np.fc = fc;
})(first_np || (first_np = {}));
///<reference path='./nsfile.ts'/>
console.log("hello");
function fun(str) {
    return `Hello ${str}`;
}
let x = fun("first ts program");
console.log(x);
let floats = 10.11;
console.log(floats);
let str1 = "pavani";
console.log(str1);
let arr = [1, 2, 3, 4];
console.log(arr);
let arr1 = ["pav", "ani", "xyz"];
console.log(arr1);
let mul_arr = [[1, 2], [3, 4]];
console.log(mul_arr);
let copy_arr1 = [...arr];
let copy_arr2 = [...arr, 5, 6];
let merge_arr = [...arr, ...arr1];
console.log(copy_arr1);
console.log(copy_arr2);
console.log(merge_arr);
let arr_obj = new Array("xd", "yd", "zd");
console.log(arr_obj);
let tup = ["pavani", 1, "techigai", 2];
console.log(`${tup[0]} working in ${tup[2]}`);
let tup1 = ["pavani", 1, "techigai", 2];
console.log(tup1);
var stats;
(function (stats) {
    stats[stats["sunday"] = 1] = "sunday";
    stats[stats["monday"] = 2] = "monday";
    stats["tuesday"] = "tue";
})(stats || (stats = {}));
console.log(stats.monday);
console.log(stats.tuesday);
class samp_class {
    constructor(name, id) {
        this.name = name;
        this.id = id;
    }
    display() {
        console.log(`name ${this.name} and id ${this.id}`);
    }
}
let samp_obj = new samp_class("pavani", 116);
samp_obj.display();
let int_obj = {};
int_obj.name = "pavani";
int_obj.company = "techigai";
console.log(int_obj.name, int_obj.company);
class cls {
    constructor() {
        this.name = "pavani";
        console.log(`${this.name} class constructor invoked`);
    }
    display(name) {
        return `${this.name} working in techigai`;
    }
}
var obj_ifc = {};
console.log(obj_ifc.display);
console.log(obj_ifc.name);
let details_obj = new first_np.fc("pavani");
console.log(details_obj.display());
