var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
///<reference path = './decfile.d.ts'/>
var BugReport = /** @class */ (function () {
    function BugReport(t) {
        this.type = "report";
        this.title = t;
    }
    BugReport = __decorate([
        sealed
    ], BugReport);
    return BugReport;
}());
function sealed(constructor) {
    console.log(constructor.name);
    console.log(constructor.prototype);
}
function fun_one() {
    console.log("one function called");
    return function (target, propertyKey, descriptor) {
        console.log("one() returned");
    };
}
function fun_two() {
    console.log("two function called");
    return function (target, propertyKey, descriptor) {
        console.log("two() returned");
    };
}
var cls_dec = /** @class */ (function () {
    function cls_dec() {
    }
    cls_dec.prototype.method = function () {
        console.log("method called");
    };
    __decorate([
        fun_one(),
        fun_two()
    ], cls_dec.prototype, "method");
    return cls_dec;
}());
var cls_dec_obj = new cls_dec();
cls_dec_obj.method();
var Greeter = /** @class */ (function () {
    function Greeter(message) {
        this.greeting = message;
    }
    Greeter.prototype.greet = function () {
        return "Hello, " + this.greeting;
    };
    __decorate([
        enumerable(false)
    ], Greeter.prototype, "greet");
    return Greeter;
}());
function enumerable(value) {
    return function (target, propertyKey, descriptor) {
        descriptor.enumerable = value;
    };
}
var gr_obj = new Greeter("pavani");
console.log(gr_obj.greet());
var Point = /** @class */ (function () {
    function Point(x, y) {
        this._x = x;
        this._y = y;
    }
    Object.defineProperty(Point.prototype, "x", {
        get: function () {
            return this._x;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "y", {
        get: function () {
            return this._y;
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        configurable(false)
    ], Point.prototype, "x");
    __decorate([
        configurable(false)
    ], Point.prototype, "y");
    return Point;
}());
function configurable(value) {
    return function (target, propertyKey, descriptor) {
        descriptor.configurable = value;
    };
}
var pt_obj = new Point(10, 20);
console.log(pt_obj.x);
console.log(pt_obj.y);
var Callable = /** @class */ (function () {
    function Callable() {
    }
    Callable.prototype.call = function () {
        console.log("Call!");
    };
    return Callable;
}());
var Activable = /** @class */ (function () {
    function Activable() {
        this.active = false;
    }
    Activable.prototype.activate = function () {
        this.active = true;
        console.log("Activating…");
    };
    Activable.prototype.deactive = function () {
        this.active = false;
        console.log("Deactivating…");
    };
    return Activable;
}());
function applyMixins(derivedCtor, baseCtors) {
    console.log("MIXINS");
    baseCtors.forEach(function (baseCtor) {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(function (name) {
            var descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name);
            Object.defineProperty(derivedCtor.prototype, name, descriptor);
        });
    });
}
var Myclass = /** @class */ (function () {
    function Myclass() {
        console.log("myclass constructor");
    }
    return Myclass;
}());
applyMixins(Myclass, [Callable, Activable]);
var mcls_obj = new Myclass();
mcls_obj.call();
mcls_obj.activate();
mcls_obj.deactive();
var propDesc = /** @class */ (function () {
    function propDesc() {
        this.num = 1;
    }
    __decorate([
        value()
    ], propDesc.prototype, "num");
    return propDesc;
}());
function value() {
    console.log("outer p d");
    return function (target, propertyKey) {
        console.log("property descriptor");
        var val = target[propertyKey];
        console.log(val + " in val");
        var setter = function () { return val = 0; };
        var getter = function () { return val; };
        Object.defineProperty(target, propertyKey, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
        });
    };
}
var propdes_obj = new propDesc();
console.log(propdes_obj.num);
console.log(a_decl);
