///<reference path = './decfile.d.ts'/>
@sealed
class BugReport {
  type = "report";
  title: string;

  constructor(t: string) {
    this.title = t;
  }
}

function sealed(constructor: Function) {
    console.log(constructor.name);
    console.log(constructor.prototype);
  }

  function fun_one(){
      console.log("one function called");
      return function(target:any,propertyKey:string,descriptor:PropertyDescriptor){
          console.log("one() returned");
      };
  }
  function fun_two(){
      console.log("two function called");
      return function(target:any,propertyKey:string, descriptor: PropertyDescriptor){
          console.log("two() returned");
      };
  }
  class cls_dec{
      @fun_one()
      @fun_two()
      method(){
          console.log("method called");
      }
  }
  let cls_dec_obj = new cls_dec();
  cls_dec_obj.method();

class Greeter {
    greeting: string;
    constructor(message: string) {
      this.greeting = message;
    }
  
    @enumerable(false)
    greet() {
      return "Hello, " + this.greeting;
    }
}
function enumerable(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      descriptor.enumerable = value;
    };
  }
let gr_obj = new Greeter("pavani");
console.log(gr_obj.greet());

class Point {
    private _x: number;
    private _y: number;
    constructor(x: number, y: number) {
      this._x = x;
      this._y = y;
    }
  
    @configurable(false)
    get x() {
      return this._x;
    }
  
    @configurable(false)
    get y() {
      return this._y;
    }
  }
  function configurable(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      descriptor.configurable = value;
    };
  }

  var pt_obj = new Point(10,20);
  console.log(pt_obj.x);
  console.log(pt_obj.y);

 class Callable {
    call() {
        console.log("Call!")
    }
}
 class Activable {
    active: boolean = false
    activate() {
        this.active = true
        console.log("Activating…")
    }
    deactive() {
        this.active = false
        console.log("Deactivating…")
    }
}
interface MyClass extends Callable, Activable{}
function applyMixins(derivedCtor: any, baseCtors: any[]) {
    console.log("MIXINS")
    baseCtors.forEach(baseCtor => {
        Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
            let descriptor = Object.getOwnPropertyDescriptor(baseCtor.prototype, name)
            Object.defineProperty(derivedCtor.prototype, name, <PropertyDescriptor & ThisType<any>>descriptor);
        });
    });
}
class Myclass{
    constructor(){
        console.log("myclass constructor");
    }
}
applyMixins(Myclass, [Callable,Activable]);
let mcls_obj = new Myclass();
mcls_obj.call();
mcls_obj.activate();
mcls_obj.deactive();

class propDesc{
  @value()
  num:number=1;
}
function value(){
  console.log("outer p d");
  return function(target:any,propertyKey:string){
    console.log("property descriptor");
    let val = target[propertyKey];
    console.log(`${val} in val`)
    const setter = ()=> val = 0;
    const getter = ()=>{ return val; }
    Object.defineProperty(target,propertyKey,{
      get: getter,
      set: setter,
      enumerable: true,
      configurable: true
    });
  };
}
let propdes_obj = new propDesc();
console.log(propdes_obj.num);

console.log(a_decl);